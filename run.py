import os

from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.interval import IntervalTrigger

from strategy.dual_gpu_alternate import DualGpuAlternate

schedule_interval = int(os.environ["STRATEGY_INTERVAL"])
scheduler = BlockingScheduler()
strategy = DualGpuAlternate(
    api_base_url=os.environ["API_BASE_URL"],
    device_ids=[int(device_id) for device_id in os.environ["DEVICE_IDS"].split(",")],
    device_min_temp=int(os.environ["DEVICE_MIN_TEMP"]),
)


@scheduler.scheduled_job(IntervalTrigger(seconds=schedule_interval))
def execute_strategy():
    print("Executing strategy")
    strategy.execute()


if __name__ == '__main__':
    scheduler.start()
