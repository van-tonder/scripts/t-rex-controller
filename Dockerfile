FROM python:3.8-slim-buster

WORKDIR /controller

COPY . .

RUN pip3 install -r requirements.txt

CMD [ "python3", "./run.py"]
