class Strategy:
    def execute(self):
        raise NotImplemented
