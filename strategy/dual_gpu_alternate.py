import requests

from strategy.strategy import Strategy


class DualGpuAlternate(Strategy):

    @staticmethod
    def _get_gpu_temp(summary, device_id):
        for gpu in summary["gpus"]:
            if gpu["device_id"] == device_id:
                return gpu["temperature"]

    def __init__(self, **kwargs):
        print("Initialising Dual GPU Alternate strategy")
        self._api_base_url = kwargs["api_base_url"]
        self._device_min_temp = kwargs["device_min_temp"]
        self._paused_device_id = kwargs["device_ids"][0]
        self._running_device_id = kwargs["device_ids"][1]
        print(
            "Dual GPU Alternate strategy initialised with: "
            f"'api_base_url'='{ self._api_base_url }' "
            f"'device_min_temp'='{ self._device_min_temp }' "
            f"'paused_device_id'='{ self._paused_device_id }' "
            f"'running_device_id'='{ self._running_device_id }'"
        )
        self._pause_device(self._paused_device_id)

    def execute(self):
        summary = self._get_summary()
        paused_gpu_temp = DualGpuAlternate._get_gpu_temp(summary, self._paused_device_id)

        if paused_gpu_temp is not None and paused_gpu_temp <= self._device_min_temp:
            print(
                f"Device with ID '{ self._paused_device_id }' has reached minimum temperature of "
                f"'{ self._device_min_temp }' - alternating devices"
            )
            self._alternate_device()

    def _get_summary(self):
        return requests.get(f"{ self._api_base_url }/summary").json()

    def _pause_request(self, device_id, pause):
        requests.get(f"{ self._api_base_url }/control", {
            "pause": (f"{ str(pause).lower() }:{ device_id }",)
        })

    def _pause_device(self, device_id):
        print(f"Pausing device with ID: '{ device_id }'")
        self._pause_request(device_id, pause=True)

    def _unpause_device(self, device_id):
        print(f"Starting device with ID: '{ device_id }'")
        self._pause_request(device_id, pause=False)

    def _alternate_device(self):
        device_id_to_run, device_id_to_pause = self._paused_device_id, self._running_device_id
        self._unpause_device(device_id_to_run)
        self._pause_device(device_id_to_pause)
        self._paused_device_id, self._running_device_id = device_id_to_pause, device_id_to_run
