# 1.0.0 (2021-04-18)


### Features

* create dual gpu strategy ([182688c](https://gitlab.com/van-tonder/scripts/t-rex-controller/commit/182688c9d384fb01bbba180633cdb22221f0f887))
